﻿using System;
using BigXmlCreator.Configs;
using BigXmlCreator.FileCreators;
using BigXmlCreator.Helpers;

namespace BigXmlCreator
{ 
	/// <summary>Предоставляет доступ к созданию больших XML</summary>
	public class BigXmlCreatorClient 
    {
		/// <summary>Создает из структурированной XML файл нужного размера.</summary>
		/// <param name="pathToXml">Путь до исходного XML.</param>
		/// <param name="neededNode">Элемент, с помощью которого будет увеличен размер.</param>
		/// <param name="number">Требуемый размер (число).</param>
		/// <param name="units">Требуемая размерность (KB, MB, GB).</param>
		/// <param name="rootNode">Корневой элемент XML.</param>
		/// <returns>Путь до созданного файла.</returns>
		public string CreateBigXmlAndGetPath(string pathToXml, string neededNode, int number, Units units, string rootNode)
		{
			var bigXmlInfo = new BigXmlInfo(pathToXml, neededNode, rootNode, number, units);
		    new _BigXmlCreator().CreateBigXml(bigXmlInfo);
		    var xmlAfterBuild = XmlNamesSetter.NewFileName;
            return xmlAfterBuild;
	    }

		/// <summary>Создает из структурированной XML файн нужного размера. Использует консольный интерфейс.</summary>
		public void CreateBigXmlFromConsole()
		{
			Console.WriteLine(
				"Введите команду запроса по следующему шаблону:\n{НазваниеФайла.СРасширением} {НазваниеЭлемента} {Размер(число)} {Размерность(KB,MB,GB)} {КорневойЭлемент}");
			var inputParams = Console.ReadLine();
			new _BigXmlCreator().CreateBigXml(new InputParamsParser(inputParams));
		}
    }
}
