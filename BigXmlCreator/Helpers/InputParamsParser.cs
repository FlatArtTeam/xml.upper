﻿using System;
using System.Collections.Generic;
using BigXmlCreator.Configs;

namespace BigXmlCreator.Helpers
{
	internal class InputParamsParser
	{
		internal string[] parsedParams;

		internal InputParamsParser(string inputParams)
		{
			parsedParams = inputParams.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
		}

		private readonly Dictionary<string, Units> _unitsAdapter = new Dictionary<string, Units>
		{
			["KB"] = Units.KB,
			["MB"] = Units.MB,
			["GB"] = Units.GB
		};

		public BigXmlInfo BigXmlInfo()
		{
			return new BigXmlInfo(
				parsedParams[0], 
				parsedParams[1],
				parsedParams[4],
				int.Parse(parsedParams[2]),
				_unitsAdapter[parsedParams[3]]);
		}
	}
}