﻿using System;
using System.IO;
using BigXmlCreator.Configs;

namespace BigXmlCreator.Helpers
{
	internal class IterationsFinder
	{
		internal long GetIterationsToNeededSize(int number, Units units)
		{
			var headersSize = GetFileLength(XmlNamesSetter.HeaderFile);
			var bodySize = GetFileLength(XmlNamesSetter.BodyFile);
			var footerSize = GetFileLength(XmlNamesSetter.FooterFile);

			var neededSizeInBytes = (long)(number * Math.Pow(2, 10 * (int)units));
			var iterations = ((neededSizeInBytes - headersSize - footerSize) / bodySize);

			return iterations;
		}

		private long GetFileLength(string fileName)
		{
			return new FileInfo(Path.GetFullPath(fileName)).Length;
		}
	}
}