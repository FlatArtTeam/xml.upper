﻿using BigXmlCreator.Configs;

namespace BigXmlCreator.Helpers
{
	internal class BigXmlInfo
	{
		public readonly string pathToXml;
		public readonly string neededNode;
		public readonly string rootNode;
		public readonly int number;
		public readonly Units unit;

		internal BigXmlInfo(string pathToXml, string neededNode, string rootNode, int number, Units unit)
		{
			this.pathToXml = pathToXml;
			this.neededNode = neededNode;
			this.rootNode = rootNode;
			this.number = number;
			this.unit = unit;
		}
	}
}