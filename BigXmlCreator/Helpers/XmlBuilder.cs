﻿using System.IO;
using System.Text;
using BigXmlCreator.Configs;
using BigXmlCreator.Helpers.Writers;

namespace BigXmlCreator.Helpers
{
	internal class XmlBuilder
	{
		private readonly IWriter cloner;
		internal XmlBuilder(IWriter cloner)
		{
			this.cloner = cloner;
		}

		internal void GetFileAfterClone()
		{
			var nameOfNewFile = XmlNamesSetter.NewFileName;
			var header = Read(XmlNamesSetter.HeaderFile);
			var body = Read(XmlNamesSetter.BodyFile);
			var footer = Read(XmlNamesSetter.FooterFile);
			var writerLines = new Cloner();

			writerLines.Write(nameOfNewFile, header);
			cloner.Write(nameOfNewFile, body);
			writerLines.Write(nameOfNewFile, footer);
		}

		private static string Read(string pathToFile)
		{
			using (var sr = new StreamReader(pathToFile, Encoding.Default))
			{
				return sr.ReadToEnd();
			}
		}
	}
}