﻿namespace BigXmlCreator.Helpers.Writers
{
	internal interface IWriter
	{
		void Write(string pathOrFileName, string file);
	}
}