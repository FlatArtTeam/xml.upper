﻿using System.IO;
using System.Text;

namespace BigXmlCreator.Helpers.Writers
{
	internal class Writer : IWriter
	{
		public void Write(string pathOrFileName, string file)
		{
			using (var sw = new StreamWriter(Path.GetFullPath(pathOrFileName), true, Encoding.Default))
			{
				sw.Write(file);
			}
		}
	}
}