﻿using System;
using System.IO;
using System.Text;

namespace BigXmlCreator.Helpers.Writers
{
	internal class Cloner : IWriter
	{
		private long iterations;

		internal Cloner(long iterations = 1)
		{
			this.iterations = iterations;
		}

		public void Write(string pathOrFileName, string file)
		{
			using (var sw = new StreamWriter(Path.GetFullPath(pathOrFileName), true, Encoding.Default))
			{
				for (var i = iterations; i > 0; i--)
					sw.WriteLine(file);
			}
		}
	}
}