﻿using System;
using System.IO;
using System.Text;

namespace BigXmlCreator.Helpers.Writers
{
	public class ClonerWithLoader : IWriter
	{
		private long iterations;

		internal ClonerWithLoader(long iterations = 1)
		{
			this.iterations = iterations;
		}
		
		public void Write(string pathOrFileName, string file)
		{
			using (var sw = new StreamWriter(Path.GetFullPath(pathOrFileName), true, Encoding.Default))
			{
				var integerPercent = 0;

				for (var i = 0; i <= iterations; i++)
				{
					sw.WriteLine(file);
					var actualPercent = i * 100 / iterations;
					if (actualPercent <= integerPercent) continue;
					Console.Write($"Загрузка... {actualPercent}%\r");
					integerPercent++;
				}
				Console.WriteLine("Загрузка завершена!");
			}
		}
	}
}