﻿using System.IO;
using BigXmlCreator.Configs;

namespace BigXmlCreator.Helpers
{
	internal class Cleaner
	{
		internal void CleaningAfterCreatedXml()
		{
			File.Delete(Path.GetFullPath(XmlNamesSetter.HeaderFile));
			File.Delete(Path.GetFullPath(XmlNamesSetter.BodyFile));
			File.Delete(Path.GetFullPath(XmlNamesSetter.FooterFile));
		}
	}
}