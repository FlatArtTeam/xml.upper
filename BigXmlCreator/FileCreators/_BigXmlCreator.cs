﻿using BigXmlCreator.Configs;
using BigXmlCreator.Helpers;
using BigXmlCreator.Helpers.Writers;

namespace BigXmlCreator.FileCreators
{
	internal class _BigXmlCreator : IBigXmlCreator
	{
		public void CreateBigXml(BigXmlInfo bigXmlInfo)
		{
			XmlNamesSetter.SetXmlNames(bigXmlInfo.pathToXml);
			var xmlParser = new XmlParser(new Writer());
			xmlParser.GetFilesWithPartsOfXml(bigXmlInfo.neededNode, bigXmlInfo.pathToXml, bigXmlInfo.rootNode);

			var iterations = new IterationsFinder().GetIterationsToNeededSize(bigXmlInfo.number, bigXmlInfo.unit);
			var xmlBuilder = new XmlBuilder(new Cloner(iterations));
			xmlBuilder.GetFileAfterClone();

			new Cleaner().CleaningAfterCreatedXml();
		}

		public void CreateBigXml(InputParamsParser inputParserParams)
		{
			var bigXmlInfo = inputParserParams.BigXmlInfo();
			XmlNamesSetter.SetXmlNames(bigXmlInfo.pathToXml);

			var xmlParser = new XmlParser(new Writer());
			xmlParser.GetFilesWithPartsOfXml(bigXmlInfo.neededNode, bigXmlInfo.pathToXml, bigXmlInfo.rootNode);

			var iterations = new IterationsFinder().GetIterationsToNeededSize(bigXmlInfo.number, bigXmlInfo.unit);
			var xmlBuilder = new XmlBuilder(new ClonerWithLoader(iterations));
			xmlBuilder.GetFileAfterClone();

			new Cleaner().CleaningAfterCreatedXml();
		}
	}
}