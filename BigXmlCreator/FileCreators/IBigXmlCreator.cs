﻿using BigXmlCreator.Helpers;

namespace BigXmlCreator.FileCreators
{
	internal interface IBigXmlCreator
	{
		void CreateBigXml(BigXmlInfo bigXmlInfo);
	}
}