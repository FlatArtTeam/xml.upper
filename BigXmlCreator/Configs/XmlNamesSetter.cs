﻿using System;
using System.IO;

namespace BigXmlCreator.Configs
{
	internal static class XmlNamesSetter 
	{
		public static string FileDirectory;
		public static string NameMode;
		public static string HeaderFile;
		public static string BodyFile;
		public static string FooterFile;
		public static string NewFileName;

		internal static void SetXmlNames(string pathToXml)
		{
			FileDirectory = $@"{Path.GetDirectoryName(Path.GetFullPath(pathToXml))}\";
			NameMode = $"{DateTime.Now:yyyyMMddhhmmss}";
			HeaderFile = $"{FileDirectory}header {NameMode}.xml";
			BodyFile = $"{FileDirectory}body {NameMode}.xml";
			FooterFile = $"{FileDirectory}footer {NameMode}.xml";
			NewFileName = $"{FileDirectory}newXml_{NameMode}.xml";
		}
	}
}