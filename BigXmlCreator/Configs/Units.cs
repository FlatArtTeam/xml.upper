﻿namespace BigXmlCreator.Configs
{
	public enum Units
	{
		KB = 1,
		MB = 2,
		GB = 3
	}
}