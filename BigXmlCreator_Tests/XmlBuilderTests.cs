﻿using System.IO;
using System.Xml.Linq;
using BigXmlCreator.Configs;
using BigXmlCreator.Helpers;
using BigXmlCreator.Helpers.Writers;
using FluentAssertions;
using NUnit.Framework;

namespace BigXmlCreator_Tests
{
	[TestFixture]
	public class XmlBuilderTests
	{
		public string xmlName;
		private XmlParser xmlParser;
		[SetUp]
		public void SetUp()
		{
			var testData = new TestData();
			xmlName = testData.GetTestXml();
			xmlParser = new XmlParser(new Writer());
		}

		[TearDown]
		public void TearDown()
		{
			File.Delete(xmlName);
			new Cleaner().CleaningAfterCreatedXml();
			File.Delete(XmlNamesSetter.NewFileName);
		}

		[Test]
		public void BuildXmlBodyHasOneNodeWithoutEnd()
		{
			xmlParser.GetFilesWithPartsOfXml("ЭлементБезЗакрывающегоТэга", xmlName, "Файл");
			var xmlBuilder = new XmlBuilder(new Cloner(3));
			xmlBuilder.GetFileAfterClone();
			var actualXml = XDocument.Parse(File.ReadAllText(XmlNamesSetter.NewFileName)).ToString();
			var expectedXml = 
				XDocument.Parse(@"
				<Файл>
					<Документ>
						<ЭлементСЗакрывающимТэгом>
							<ЭлементБезЗакрывающегоТэга ФИО=""Тестовый тестер""/>
							<ЭлементБезЗакрывающегоТэга ФИО=""Тестовый тестер""/>
							<ЭлементБезЗакрывающегоТэга ФИО=""Тестовый тестер""/>
							</ЭлементСЗакрывающимТэгом>
							</Документ>
							</Файл>").ToString();
			actualXml.Should().Be(expectedXml);
		}

		[Test]
		public void BuildXmlBodyHasOneNode()
		{
			xmlParser.GetFilesWithPartsOfXml("ЭлементСЗакрывающимТэгом", xmlName, "Файл");
			var xmlBuilder = new XmlBuilder(new Cloner(2));
			xmlBuilder.GetFileAfterClone();
			var actualXml = XDocument.Parse(File.ReadAllText(XmlNamesSetter.NewFileName)).ToString();
			var expectedXml =
				XDocument.Parse(@"
				<Файл>
					<Документ>
						<ЭлементСЗакрывающимТэгом>
							<ЭлементБезЗакрывающегоТэга ФИО=""Тестовый тестер""/>
							</ЭлементСЗакрывающимТэгом>
							<ЭлементСЗакрывающимТэгом>
							<ЭлементБезЗакрывающегоТэга ФИО=""Тестовый тестер""/>
							</ЭлементСЗакрывающимТэгом>
							</Документ>
							</Файл>").ToString();
			actualXml.Should().Be(expectedXml);
		}
	}
}