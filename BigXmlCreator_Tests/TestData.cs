﻿using System.Xml.Linq;
using BigXmlCreator.Configs;
using NUnit.Framework;

namespace BigXmlCreator_Tests
{
	public class TestData
	{
		public string GetTestXml()
		{
			var testFile = "test.xml";
			XmlNamesSetter.SetXmlNames(TestContext.CurrentContext.TestDirectory);
				var xmlDoc = new XDocument(new XElement("Файл",
					new XElement("Документ",
						new XElement("ЭлементСЗакрывающимТэгом",
							new XElement("ЭлементБезЗакрывающегоТэга",
								new XAttribute("ФИО", "Тестовый тестер"))
						))));
			xmlDoc.Save(testFile);
			return testFile;
		}
	}
}