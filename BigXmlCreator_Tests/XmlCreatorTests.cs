﻿using System.IO;
using BigXmlCreator.Configs;
using BigXmlCreator.FileCreators;
using BigXmlCreator.Helpers;
using NUnit.Framework;

namespace BigXmlCreator_Tests
{
	[TestFixture]
	public class XmlCreatorTests
	{
		private BigXmlInfo bigXmlInfo;
		private string xmlName;
		[SetUp]
		public void SetUp()
		{
			var testData = new TestData();
			xmlName = testData.GetTestXml();
			bigXmlInfo = new BigXmlInfo(
				xmlName,
				"ЭлементСЗакрывающимТэгом", 
				"Файл",
				100,
				Units.KB);
		}

		[TearDown]
		public void TearDown()
		{
			File.Delete(xmlName);
			File.Delete(XmlNamesSetter.NewFileName);
		}

		[Test]
		public void CheckFileLength()
		{
			var creator = new _BigXmlCreator();
			creator.CreateBigXml(bigXmlInfo);
			Assert.AreEqual(new FileInfo(XmlNamesSetter.NewFileName).Length, 103255);
		}
	}
}