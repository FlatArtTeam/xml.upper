using System.IO;
using System.Xml.Linq;
using BigXmlCreator.Configs;
using BigXmlCreator.Helpers;
using BigXmlCreator.Helpers.Writers;
using FluentAssertions;
using NUnit.Framework;

namespace BigXmlCreator_Tests
{
	[TestFixture]
	public class XmlParserTests
	{
		public string xmlName;
		[SetUp]
		public void SetUp()
		{
			var testData = new TestData();
			xmlName = testData.GetTestXml();
		}

		[TearDown]
		public void TearDown()
		{
			File.Delete(xmlName);
			new Cleaner().CleaningAfterCreatedXml();
		}

		[Test]
		public void ParseXmlWithEndElement()
		{
			var xmlParser = new XmlParser(new Writer());
			xmlParser.GetFilesWithPartsOfXml("������������������������", xmlName, "����");
			var actualXml = XDocument.Parse(File.ReadAllText(XmlNamesSetter.BodyFile)).ToString();
			var expectedXml =
				XDocument.Parse(
					@"<������������������������><�������������������������� ���=""�������� ������""/></������������������������>").ToString();
			actualXml.Should().Be(expectedXml);
		}

		[Test]
		public void ParseXmlWithoutEndElement()
		{
			var xmlParser = new XmlParser(new Writer());
			xmlParser.GetFilesWithPartsOfXml("��������������������������", xmlName, "����");
			var actualXml = XDocument.Parse(File.ReadAllText(XmlNamesSetter.BodyFile)).ToString();
			var expectedXml =
				XDocument.Parse(
					@"<�������������������������� ���=""�������� ������""/>").ToString();
			actualXml.Should().Be(expectedXml);
		}
	}
}